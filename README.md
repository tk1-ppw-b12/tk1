# TK1

**KELOMPOK KB-12
KELAS B**


Anggota Kelompok:
- Gilbert Stefano Wijaya
- Ilma Ainur Rohma
- Nabila Azzahra
- Fauzan Pradana Linggih

Link Herokuapp:


Aplikasi yang dibuat:
Web ini dibuat dengan latar belakang ketika ada orang yang ingin membangun rumah tetapi sulit menemukan kontraktor atau pekerja untuk membangun rumahnya. web ini hadir untuk menghubungan orang yang ingin membangun rumah dan kontraktor atau pekerja. Orang yang ingin membangun rumah atau user akan mengisi form request dengan ketentuan yang ada, kemudian ia menunggu smapai ada kontraktor yang ingin membangun rumahnya. Dari sisi kontraktor akan diberikan list user request beserta ketentuannya, ia tinggal memilih ingin mengerjakan proyek yang mana dengan mengisi form yang ada pula. Web ini akan ada mitra kontraktor untuk melayanin user dengan cepat, namun kontraktor indie atau kontraktor kecil dapat juga menggunakan web ini untuk mencari proyek. Kedepannya web seperti ini diharapkan mampu terintegrasi dengan suatu tekonologi yang maju seperti 3D printer, maka User hanya tinggal mengisi ketentuan yang ada lalu langsung proses membangun rumah.

Daftar Fitur:
- Home Page
- User side
- Contractor side
- Review Page (Terdapat form yang berisi nama email, rating dan komen pengguna untuk aplikasi, komentar komentar yang sudah ada juga di tampilkan dalam halaman ini)
